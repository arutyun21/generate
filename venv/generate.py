import random
import argparse
import sys
import pickle


def build_sentence(pairs, seed, length):
    result = []
    for i in range(length):
        # идем по всем парам
        result.append(seed + ' ')
        # записываем наше слово
        frequency = []
        # следующие слова за seed
        for words in pairs:
            if words[0] == seed:  # нашли пару с seed-world[]
                for k in range(pairs.get(words)):
                    frequency.append(words[1])
                    # записываем в freq слова следующие
                    # за seed(сколько , сколько вхождений пар)
        seed = random.choice(frequency)  # выбираем следующее
    return ''.join(result)


def CreateParser():
    parser = argparse.ArgumentParser(description='Generate text from model.')
    parser.add_argument(
        '--seed',
        dest='seed',
        type=str,
        default=None,
        help='starting word (default: random)')
    parser.add_argument(
        '--output',
        dest='output',
        type=argparse.FileType('w', encoding='utf-8'),
        default=sys.stdout,
        help='output model (default: stdout)')
    requiredNamed = parser.add_argument_group('required arguments')
    requiredNamed.add_argument(
        '--model',
        dest='model',
        type=argparse.FileType('rb'),
        help='input model',
        required=True)
    requiredNamed.add_argument(
        '--length',
        dest='length',
        type=int,
        help='length of the sequence to be generated',
        required=True)
    return parser


def ProcessInput(model):
    pairs = pickle.load(model)

    if args.seed is not None:
        seed = args.seed
    else:
        seed = random.choice(list(pairs.keys()))[0]

    return build_sentence(pairs, seed, args.length)


if __name__ == '__main__':
    args = CreateParser().parse_args()
    args.output.write(ProcessInput(args.model))

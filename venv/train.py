import os
import argparse
import sys
import re
import pickle
from collections import defaultdict


def make_model(path, low_case, model):
    prev = ''
    for line in path:  # читаем из файла
        line = line.strip()  # убираем пробелы сначала и с конца
        reg = re.compile('[^a-zA-Zа-яА-Я-ё ]')  # очищаем от неалфавитных
        line = reg.sub('', line)  # символов
        if not line:
            continue
        if low_case:  # приводим  к lowercase, если надо
            line = line.lower()
        words = []
        if prev != '':
            words.append(prev)
        words += line.split()  # множество пар
        # прочитали из файла
        prev = words[-1]
        # идем по всем словам , кроме поледнего, тк у него нет пары
        for i in range(len(words) - 1):
            model[(words[i], words[i + 1])] += 1  # записавыем пары слов


def CreateParser():
    parser = argparse.ArgumentParser(description='Generate text model.')
    parser.add_argument(
        '--input-dir',
        dest='input_dir',
        type=str,
        default="",
        help='input files dir (default: stdin)')
    parser.add_argument(
        '--lc',
        dest='lc',
        action='store_true',
        help='make whole text lowercase')

    requiredNamed = parser.add_argument_group('required arguments')
    requiredNamed.add_argument(
        '--model',
        dest='model',
        type=argparse.FileType('wb'),
        help='output file',
        required=True)
    return parser


def ProcessInput(args):
    model = defaultdict(lambda: 0)
    if not args.input_dir:
        make_model(sys.stdin, args.lc, model)
    else:
        for cur_file in os.listdir(args.input_dir):
            mfile = open(
                os.path.join(args.input_dir, cur_file), encoding='utf-8')
            make_model(mfile, args.lc, model)
            mfile.close()
    return model


if (__name__ == "__main__"):
    args = CreateParser().parse_args()
    model = dict(ProcessInput(args))
    out_pickle = pickle.dumps(model)
    args.model.write(out_pickle)
